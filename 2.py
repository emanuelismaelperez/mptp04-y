import os

# Menu del programa 
def menu():
    print('1) Registrar productos')
    print('2) Mostrar el listado de productos')
    print('3) Mostrar los productos cuyo stock se encuentre en el intervalo [desde, hasta]')
    print('4) Diseñar un proceso que le sume X al stock de todos los productos cuyo valor actual de stock sea menor al valor Y.')
    print('5) Eliminar todos los productos cuyo stock sea igual a cero.')
    print('6) Salir')
    opcion = int(input('Ingrese una opción: '))
    while not (opcion >= 1 and opcion <= 6): 
        opcion = int(input('Error opcion no valida. Ingrese una opción: '))
    return opcion    

def validarX(x):
    x=int(input('Eror. Deve ingresar un valor poitivo: ')) 
    while  0>x :
       x = int(input('Eror. Deve ingresar un valor poitivo: '))
    return x    

def buscarClave(diccionario,clave):
    datos=diccionario.get(clave,-1)
    return datos

def leerProductos():
    productos =    {88:['pera', 10.0, 4],
                    29:['Anana', 2.0, 0],
                    53:['naranja', 7.5, 3],
                    67:['frutlla',6.5, 6]}
    respuesta = 's'
    while respuesta == 's':
        codigo = int(input('Cogigo: '))
        dato = buscarClave(productos,codigo)
        while dato != -1: 
            codigo = int(input('Cogigo: '))
            dato = buscarClave(productos,codigo)      
        nombre = input('Nombre: ')
        precio = float(input('Precio : $'))
        if precio<0:
            precio=validarX(precio)
        stock = int(input('Stock: '))
        if stock<0:
            stock=validarX(stock)
        productos[codigo] = [nombre,precio,stock]
        print('agregado correctamente')
        respuesta = input('Continuar?: ')
    return productos   
    
def mostrar(diccionario):
    print('Listado de Productos')
    for clave, valor in diccionario.items():
        print(clave,valor)

def mostrarDesdeHasta(diccionario):
    desde = int(input('stock desde: '))
    hasta = int(input('stock hasta: '))
    while  not desde<hasta:
        desde = int(input('stock desde: '))
        hasta = int(input('stock hasta: '))
    for clave, valor in diccionario.items():
        if desde<=valor[2]<=hasta:
            print(clave,valor)

def sumaStock(productos):
    y = int(input('Ingrese el valor de stock menores a este a incrementar: '))
    x = int(input('Cantida de stock a incrementar : '))
    for clave, valor in productos.items():
        if valor[2]<y:
            valor[2]+=x
    print('Se han incremnatado los stock')
    return productos

def eliminarElemtos(diccionario,codigos):
    for item in codigos:
        del diccionario[item]
    return diccionario

def eliminarStock(productos):
    codigos=[] 
    for clave,valor in productos.items():    
        if valor[2]==0:
            codigos.append(clave)
    print('Se han eliminado los stock=0')
    return eliminarElemtos(productos,codigos)

os.system('cls')
opcion = 0
bandera= False
while opcion != 6: 
    opcion = menu() 	
    if opcion == 1: 
        productos=leerProductos()
        bandera=True
    elif opcion == 2 and bandera:
        mostrar(productos)
    elif opcion == 3 and bandera:
        mostrarDesdeHasta(productos)
    elif opcion == 4 and bandera:
        productos=sumaStock(productos)
    elif opcion == 5 and bandera:
	    productos=eliminarStock(productos)
    elif opcion == 6: 
        print('Fin del programa') 
    else:
        print('Deve realizar la opcion 1) primero') 